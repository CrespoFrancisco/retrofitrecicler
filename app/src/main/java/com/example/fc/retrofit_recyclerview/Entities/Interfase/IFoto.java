package com.example.fc.retrofit_recyclerview.Entities.Interfase;

import com.example.fc.retrofit_recyclerview.Entities.Foto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Created by francisco.crespo on 4/9/2018.
 */

public interface IFoto {
    @GET("photos")
    Call<List<Foto>> getPhotosGet();
    @POST("photos")
    Call<List<Foto>> getPhotosPost();
}
