package com.example.fc.retrofit_recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.fc.retrofit_recyclerview.Entities.Foto;
import com.example.fc.retrofit_recyclerview.Entities.Interfase.IFoto;
import com.example.fc.retrofit_recyclerview.utils.RecyclerAdapter;


import java.util.ArrayList;
import java.util.List;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity  {
    private RecyclerView recyclerView;
    private List<Foto> fotoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Peticion();



    }




    public void Peticion (){

            final String url ="https://jsonplaceholder.typicode.com/";

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            IFoto ifoto = retrofit.create(IFoto.class);

            Call<List<Foto>> respuesta = ifoto.getPhotosGet();
            respuesta.enqueue(new Callback<List<Foto>>() {
                @Override
                public void onResponse(Call<List<Foto>> call, Response<List<Foto>> response) {
                     fotoList = response.body();
                     for(Foto f :fotoList){
                         Log.e("Prueba ", f.getTitle());
                     }

                     showList();

                }

                @Override
                public void onFailure(Call<List<Foto>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

    }

    private void showList() {
        recyclerView = (RecyclerView) findViewById(R.id.main_recycler_fotos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(fotoList);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
