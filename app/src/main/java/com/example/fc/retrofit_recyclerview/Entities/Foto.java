package com.example.fc.retrofit_recyclerview.Entities;

import com.example.fc.retrofit_recyclerview.Entities.Interfase.IFoto;

import java.util.List;

import retrofit2.Call;

/**
 * Created by francisco.crespo on 4/9/2018.
 */

public class Foto implements IFoto {


    private int albumID;
    private int id;
    private String title,url,thumbnailUrl;

    public int getAlbumID() {
        return albumID;
    }

    public void setAlbumID(int albumID) {
        this.albumID = albumID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public String toString() {
        return "Foto{" +
                "albumID=" + albumID +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                '}';
    }


    @Override
    public Call<List<Foto>> getPhotosGet() {
        return null;
    }

    @Override
    public Call<List<Foto>> getPhotosPost() {
        return null;
    }
}
