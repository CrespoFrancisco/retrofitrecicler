package com.example.fc.retrofit_recyclerview.utils;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fc.retrofit_recyclerview.Entities.Foto;
import com.example.fc.retrofit_recyclerview.R;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolderAdapter> {

    List<Foto> listaFoto;
    public RecyclerAdapter(List<Foto> listaFoto) {
        this.listaFoto = listaFoto;
    }


    @Override
    public ViewHolderAdapter onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_foto,null,false);
        return new ViewHolderAdapter(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderAdapter holder, int position) {
        holder.asignarDatos(listaFoto.get(position));
    }

    @Override
    public int getItemCount() {
        return listaFoto.size();
    }

    public class ViewHolderAdapter extends RecyclerView.ViewHolder {
        TextView title,url;
        TextView thumbnail;
        public ViewHolderAdapter(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.list_item_title);
            url = (TextView) itemView.findViewById(R.id.list_item_url);
            thumbnail = (TextView) itemView.findViewById(R.id.list_item_thumbnail);
        }

        public void asignarDatos(Foto foto) {
            title.setText( foto.getTitle());
            url.setText(foto.getUrl());

            thumbnail.setText(foto.getThumbnailUrl());
        }
    }
}
